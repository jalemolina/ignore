Ignore
========
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-blue.svg)](https://gitlab.com/jalemolina/ignore)

It helps you generate the `.gitignore` file for your projects.

## Requirements

### Optional

`fzf`

## Installation (GNU-Linux) ##

If you use a profile file like `.bashrc`, `.zshrc` or `.<shell>rc`:

```sh
git clone https://gitlab.com/jalemolina/ignore.git ~/.tmpig && ~/.tmpig/ignore.sh -i && rm -Rf ~/.tmpig/
```

If you use a different profile file, you must manually add the `PATH` to the end of the file:

```sh
PATH=$PATH:$HOME/.scripts
```

## Usage ##

### Sintax ###

```sh
ignore [-a|--(no-)append] [-f|--fzf] [-u|--update] [-U|--update-all] [-i|--install] [-l|--list] [-v|--version] [-h|--help] <type-1> [<type-2>] ... [<type-n>] ..
```

### Parameters ###

| PARAM                           | DESCRIPTION                                                                                                                                                                |
|---------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `<type>`                        | File types to ignore (is case sensitive).                                                                                                                                  |
| `-a`, `--append`, `--no-append` | Add the specified types to an existing `.gitignore` file. If this option is not specified, the `.gitignore` file is created or replaces the existing one (off by default). |
| `-l`, `--list`                  | List of `.gitignore` file types.                                                                                                                                           |
| `-f`, `--fzf`                  | Use fzf search, multiselect and add types.                                                                                                                                           |
| `-u`, `--update`                | Update the submodule with the list of `.gitignore` file types.                                                                                                             |
| `-U`, `--update-all`            | Update the script to the last version and the list of `.gitignore` file types.                                                                                             |
| `-i`, `--install`               | Install the script and the list of `.gitignore` file types.                                                                                                                |
| `-v`, `--version`               | Prints version.                                                                                                                                                            |
| `-h`, `--help`                  | Prints help.                                                                                                                                                               |

### Examples ###

List all supported `.gitignore` file types:

```sh
ignore -l | less
``` 

To ignore the files of a _python_ project, working with the editors _Emacs_ and _VisualStudioCode_:

```sh
ignore Python Emacs VisualStudioCode
``` 

To also ignore the temporary files of the _Vim_ editor by adding it to the current `.gitignore`:

```sh
ignore -a Vim
``` 

To update the [gitignore](https://github.com/github/gitignore) repository submodule:

```sh
ignore -u
``` 

Check if there is a specific type:
 
```sh
ignore -l | grep -i python
``` 

-----------------------------------------------------------------------------------

 [![Made with: Bash](https://img.shields.io/badge/made%20with-bash-FFD500?style=for-the-badge)](https://www.gnu.org/software/bash)
 [![Thanks to: gitignore](https://img.shields.io/badge/Thanks%20to-gitignore-181717?logo=github&style=for-the-badge)](https://github.com/github/gitignore)

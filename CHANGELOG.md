# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) ([spanish version](https://keepachangelog.com/es-ES/1.0.0/)),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Changed
- New temp dir for installation.

### Fixed
- Now the option ` -U | --update-all` is in the help
- Organized list of options.

## [0.0.7] - 2019-12-21
### Added
- Added reference to the gitignore project in the help text.

### Fixed
- Change reference relative to the submodule path by absolute reference.

## [0.0.6] - 2019-12-20
### Added
- `-U | --update-all` option to update the entire project or submodule separately. 

### Changed
- README with the new parameter `-U`.

### Fixed
- Added to the update functions `<remote>` and `<branch>`

## [0.0.5] - 2019-12-20
### Added
- `CHANGELOG.md` file.
- An example in the README on how to check a particular type of `.gitignore`.

### Changed
- Version++
- README in the parameters, the case sensitivity is specified.

[Unreleased]: https://gitlab.com/jalemolina/ignore/compare/v0.0.7...master
[0.0.7]: https://gitlab.com/jalemolina/ignore/compare/v0.0.7...master
[0.0.6]: https://gitlab.com/jalemolina/ignore/compare/v0.0.6...master
[0.0.5]: https://gitlab.com/jalemolina/ignore/compare/v0.0.5...master
[0.0.4]: https://gitlab.com/jalemolina/ignore/compare/v0.0.4...master
[0.0.3]: https://gitlab.com/jalemolina/ignore/compare/v0.0.3...master
[0.0.2]: https://gitlab.com/jalemolina/ignore/compare/v0.0.2...master
[0.0.1]: https://gitlab.com/jalemolina/ignore/compare/v0.0.1...master
